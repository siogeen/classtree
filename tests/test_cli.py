"""
Tests for command line interface (CLI).
"""
import unittest
import os
import sys

from importlib import import_module
from os import linesep
from unittest.mock import patch

from cli_test_helpers import ArgvContext, shell

SRC_PATH = os.path.join(os.path.dirname(__file__), '..')
sys.path.insert(0, SRC_PATH)
from classtree import cli, __version__

class TestClassTreeCli(unittest.TestCase):
    """classtree cli unit tests"""

    @staticmethod
    def shell(cmd):
        return shell(cmd, cwd=SRC_PATH)

    def test_main_module(self):
        """
        Exercise (most of) the code in the ``__main__`` module.
        """
        import_module('classtree.__main__')

    def test_runas_module(self):
        """
        Can this package be run as a Python module?
        """
        result = TestClassTreeCli.shell('python3 -m classtree --help')
        self.assertEqual(0, result.exit_code, result.stderr)

    @unittest.skip
    def test_entrypoint(self):
        """
        Is entrypoint script installed? (setup.py)
        """
        result = TestClassTreeCli.shell('pyclasstree --help')
        self.assertEqual(0, result.exit_code, result.stderr)

    @patch('classtree.cli.dispatch')
    def test_usage(self, mock_dispatch):
        """
        Does CLI abort w/o arguments, displaying usage instructions?
        """
        with ArgvContext('classtree'), self.assertRaises(SystemExit):
            cli.main()
    
        self.assertFalse(mock_dispatch.called, 'CLI should stop execution')
    
        result = TestClassTreeCli.shell('python3 -m classtree')
    
        self.assertTrue('usage:' in result.stderr)

    def test_version(self):
        """
        Does --version display information as expected?
        """
        expected_version = __version__
        result = TestClassTreeCli.shell('python3 -m classtree --version')
    
        self.assertEqual(result.stdout, f"{expected_version}{linesep}")
        self.assertEqual(result.exit_code, 0, result.stderr)

    def test_one_class(self):
        """
        Is one class analysed?
        """
        #with ArgvContext('classtree', 'inspect'):
        #    args = cli.parse_arguments()
    
        #self.assertEqual(['inspect'], args.classes)
        result = TestClassTreeCli.shell(
            'python3 -m classtree test_module.sub_module.SubClass')
        self.assertEqual(0, result.exit_code, result.stderr)
        self.assertTrue(result.stdout.startswith('SubClass\n'), result.stdout)

    def test_two_classes(self):
        """
        Are two classes analysed?
        """
        result = TestClassTreeCli.shell(
            'python3 -m classtree test_module.TestClass test_module.sub_module.SubClass')
        self.assertEqual(0, result.exit_code, result.stderr)
        self.assertTrue(result.stdout.startswith('TestClass\n'), result.stdout)
        self.assertTrue('\nSubClass\n' in result.stdout, result.stdout)

    def test_func_search(self):
        """
        Are functions found?
        """
        result = TestClassTreeCli.shell(
            'python3 -m classtree --funcs .*sub.* test_module.sub_module')
        self.assertEqual(0, result.exit_code, result.stderr)
        self.assertTrue(result.stdout.startswith('sub_module\n'), result.stdout)
        self.assertTrue('-F: sub_file_fnc\n' in result.stdout, result.stdout)
        self.assertTrue('-S: ' not in result.stdout, result.stdout)

    def test_var_search(self):
        """
        Are variables found?
        """
        result = TestClassTreeCli.shell(
            'python3 -m classtree --search .*memb_var.* test_module.sub_module')
        self.assertEqual(0, result.exit_code, result.stderr)
        self.assertTrue(result.stdout.startswith('sub_module\n'), result.stdout)
        self.assertTrue('-S: sub_class_fnc\n' in result.stdout, result.stdout)
        self.assertTrue('-F: ' not in result.stdout, result.stdout)
        self.assertTrue('-memb_var\n' in result.stdout, result.stdout)

    def test_func_var_search(self):
        """
        Are functions and variables found?
        """
        result = TestClassTreeCli.shell(
            'python3 -m classtree --funcs .*sub.* --search .*memb_var.* test_module.sub_module')
        self.assertEqual(0, result.exit_code, result.stderr)
        self.assertTrue(result.stdout.startswith('sub_module\n'), result.stdout)
        self.assertTrue('-F: sub_file_fnc\n' in result.stdout, result.stdout)
        self.assertTrue('-S: __init__\n' in result.stdout, result.stdout)
        self.assertTrue('-memb_var\n' in result.stdout, result.stdout)


if __name__ == "__main__":
    unittest.main()
