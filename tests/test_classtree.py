import unittest
import os
import sys
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..'))
import classtree
import copy
import pprint

import test_module

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
OUT_DIR = os.path.join(THIS_DIR, 'output')
if not os.path.exists(OUT_DIR):
    os.mkdir(OUT_DIR)

class TestClassTree(unittest.TestCase):
    """classtree unit tests"""

    @staticmethod
    def sorted_tree_ret(obj):
        if isinstance(obj, dict):
            ret = dict(sorted(obj.items()))
            for k, v in ret.items():
                ret[k] = TestClassTree.sorted_tree_ret(v)
        elif isinstance(obj, tuple):
            ret = tuple(TestClassTree.sorted_tree_ret(t) for t in obj)
        elif isinstance(obj, list):
            ret = sorted(obj)
        else:
            raise TypeError()

        return ret

    def assertEqualTreeRet(self, d1, d2):
        sd1 = TestClassTree.sorted_tree_ret(d1)
        sd2 = TestClassTree.sorted_tree_ret(d2)
        if sd1 != sd2:
            print('Not equal:')
            print('# First:')
            pprint.pprint(sd1, indent=0)
            print('# Second:')
            pprint.pprint(sd2, indent=0)
        self.assertEqual(sd1, sd2)

    def test_analyse_and_print_class_name(self):
        tree = classtree.ClassTree('test_module.TestClass')
        self.assertFalse(tree.func_result)
        self.assertFalse(tree.search_result)
        tree.print_tree()
        print()

        tree = classtree.ClassTree('test_module.TestClass', inner_classes=True)
        self.assertFalse(tree.func_result)
        self.assertFalse(tree.search_result)
        tree.print_tree()
        print()

        tree.search(funcs='.*test_.*', search='.*memb_var.*')
        self.assertTrue(tree.func_result)
        self.assertTrue(tree.search_result)
        tree.print_tree()
        print()

        tree = classtree.ClassTree(test_module.TestClass, funcs='undef', search='undef')
        self.assertFalse(tree.func_result)
        self.assertFalse(tree.search_result)
        tree.print_tree()
        print()

        tree.search(funcs='.*test_.*', search='.*memb_var.*')
        self.assertTrue(tree.func_result)
        self.assertTrue(tree.search_result)
        tree.print_tree()
        print()

    def test_analyse_and_print_module(self):
        # sub module
        tree = classtree.ClassTree(test_module.sub_module)
        tree.search(funcs='.*test_.*', search='.*memb_var.*')
        tree.print_tree()
        print()

        # module with sub modules
        tree = classtree.ClassTree('test_module')
        names_tree = { 'test_module': {
                         'test_file': {'TestClass': {
                                            'TestSubClass1': {},
                                            'TestSubClass2': {},
                                            'TestSubMultiClass': {
                                                'TestSubSubClass': {},
                                                },
                                            },
                                       '_TestClass': {
                                            'TestSubMultiClass': {},
                                            },
                                       },
                         'sub_module': {
                             'sub_file': {'SubClass': {},
                                          '_SubClass': {}},
                         },
                       },
                     }
        tree.print_tree()
        print()
        self.assertEqualTreeRet(names_tree, tree.tree)

        tree_inner = classtree.ClassTree('test_module', inner_classes=True)
        tree_inner.print_tree()
        print()
        names_tree_inner = copy.deepcopy(names_tree)
        names_tree_inner['test_module']['test_file']['TestClass']['InnerClass'] = {}
        self.assertEqualTreeRet(names_tree_inner, tree_inner.tree)

        tree = classtree.ClassTree(test_module, funcs='.*test_.*', search=['.*memb_var.*', 'arg1'])
        func_res = {'test_module': (['test_module_fnc1'],
                        {'test_file': (['test_file_fnc1', '_test_file_fnc1'],
                            {'TestClass': (['test_class_fnc1',
                                            'test_class_method1',
                                            'test_static_class_fnc1',
                                            'test_async_func_1'],
                                {'TestSubClass1': (['test_sub_class_fnc1'], {}),
                                 'TestSubClass2': (['test_sub_class_fnc1'], {}),
                                 'TestSubMultiClass': (['test_sub_multi_class_fnc1'],
                                    {'TestSubSubClass': (['test_sub_sub_class_fnc1'], {}),
                                    }),
                                 'InnerClass': (['test_inner_class_fnc1',
                                                 'inner_class_fnc2:test_inner_func'], {}),
                                }),
                             '_TestClass': (['test_class_fnc1'], {}),
                            }),
                        }),
                   }
        srch_res = {'test_module': ({'module_fnc2': ['arg1']},
                        {'test_file': ({'file_fnc2': ['arg1'],
                                        '_file_fnc2': ['arg1'],
                                       },
                            {'TestClass': ({'__init__': ['memb_var1', 'memb_var2'],
                                           'class_fnc2': ['memb_var2'],
                                           'async_func_2': ['memb_var1'],
                                           'async_func_3': ['memb_var2'],
                                           'async_func_4': ['memb_var2'],
                                           },
                                {'TestSubClass1': ({'sub_class_fnc2': ['memb_var2']}, {}),
                                 'TestSubClass2': ({'sub_class_fnc2': ['memb_var2']}, {}),
                                 'TestSubMultiClass': ({'sub_multi_class_fnc2': ['memb_var2']},
                                    {'TestSubSubClass': ({'sub_sub_class_fnc2': ['memb_var2']}, {}),
                                    }),
                                 'InnerClass': ({'__init__': ['memb_var_inner'],
                                                 'inner_class_fnc3': ['memb_var_inner']}, {}),
                                }),
                             '_TestClass': ({'__init__': ['memb_var1', 'memb_var2'],
                                            'class_fnc2': ['memb_var2']}, {}),
                            }),
                          'sub_module': ({},
                                {'sub_file': ({'sub_file_fnc': ['arg1'],
                                               '_sub_file_fnc': ['arg1']},
                                    {'SubClass': ({'__init__': ['memb_var'],
                                                   'sub_class_fnc': ['memb_var']}, {}),
                                     '_SubClass': ({'__init__': ['memb_var'],
                                                   'sub_class_fnc': ['memb_var']}, {}),
                                    }),
                                }),
                          }),
                    }
        tree.print_tree()
        print()
        self.assertEqualTreeRet(names_tree, tree.tree)
        self.assertEqualTreeRet(func_res, tree.func_result)
        self.assertEqualTreeRet(srch_res, tree.search_result)

        tree_inner.search(funcs='.*test_.*', search=['.*memb_var.*', 'arg1'])
        tree_inner.print_tree()
        print()
        self.assertEqualTreeRet(func_res, tree.func_result)
        self.assertEqualTreeRet(srch_res, tree.search_result)

    def test_save_txt(self):
        tree = classtree.ClassTree('test_module', funcs='.*test_.*',
                                   search=['.*memb_var.*', 'arg1'])
        fname = os.path.join(OUT_DIR, 'test_tree_with_search.txt')
        tree.save(fname)
        with open(fname, 'r') as file:
            class_search_lines = file.readlines()
        self.assertEqual("".join(class_search_lines), tree.get_tree_string())

        fname = os.path.join(OUT_DIR, 'test_tree.txt')
        tree.save(fname, funcs=False, search=False)
        with open(fname, 'r') as file:
            class_lines = file.readlines()
        self.assertTrue(len(class_lines) < len(class_search_lines))

    def test_graph(self):
        tree = classtree.ClassTree('test_module', funcs='.*test_.*',
                                   search=['.*memb_var.*', 'arg1'], inner_classes=True)
        graph = classtree.TreeGraph(tree)
        graph.save(os.path.join(OUT_DIR, 'test_tree.png'))
        graph.save(os.path.join(OUT_DIR, 'test_tree_size.png'), width=2000, height=1000)

        graph.save(os.path.join(OUT_DIR, 'test_tree_auto.png'), layout='auto', width=2000, height=2000)

        graph.save(os.path.join(OUT_DIR, 'test_tree_auto_cairo.png'), plotlib='cairo', layout='auto')
        graph.save(os.path.join(OUT_DIR, 'test_tree_auto_matplotlib.png'), plotlib='matplotlib', layout='auto')

if __name__ == "__main__":
    unittest.main()
