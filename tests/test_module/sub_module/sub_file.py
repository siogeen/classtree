def sub_file_fnc(arg1, arg3):
    return arg1+arg3

class SubClass:
    """Test Sub class"""

    def __init__(self):
        self.memb_var = None

    def sub_class_fnc(self):
        self.memb_var = 5


def _sub_file_fnc(arg1, arg3):
    return arg1+arg3

class _SubClass:
    """Test Sub class"""

    def __init__(self):
        self.memb_var = None

    def sub_class_fnc(self):
        self.memb_var = 5
