import asyncio

from . import sub_module


def test_file_fnc1(arg1, arg3):
    return arg1+arg3

def file_fnc2(arg1, arg2):
    return arg1+arg2

def _test_file_fnc1(arg1, arg3):
    return arg1+arg3

def _file_fnc2(arg1, arg2):
    return arg1+arg2


class TestClass:
    """Test class"""

    class InnerClass:
        """Inner class"""

        def __init__(self):
            self.memb_var_inner = 7

        def test_inner_class_fnc1(self):
            print(5)

        def inner_class_fnc2(self):
            def test_inner_func():
                print(7)

            print(5)
            test_inner_func()

        def inner_class_fnc3(self):
            def inner_sub_func():
                self.memb_var_inner = 3

            print(5)
            inner_sub_func()

    def __init__(self):
        self.memb_var1 = None
        self.memb_var2 = sub_module.sub_file.SubClass()
        self.var3 = TestClass.InnerClass()

    def test_class_fnc1(self):
        try:
            for i in range(5):
                if i < 3:
                    self.memb_var1 = i
        except Exception as error:
            print(5)
            raise error

    def class_fnc2(self):
        while self.memb_var2 < 3:
            self.memb_var2 += 1

    async def test_async_func_1(self):
        print('Velotio ...')
        await asyncio.sleep(1)
        print('... Technologies!')

    async def async_func_2(self):
        print('Velotio ...')
        await asyncio.sleep(self.memb_var1)
        print('... Technologies!')

    async def async_func_3(self):
        async for x in self.async_func_2():
            self.memb_var2 = x

    async def async_func_4(self):
        async with self.async_func_2():
            self.memb_var2 += 1

    async def async_func_5(self):
        async with self.async_func_2():
            print(3)

    @staticmethod
    def test_static_class_fnc1():
        print(3)

    @classmethod
    def test_class_method1(cls):
        print(cls)


class TestSubClass1(TestClass):
    """Test sub class 1"""

    def test_sub_class_fnc1(self):
        self.memb_var1 = 5

    def sub_class_fnc2(self):
        self.memb_var2 = 5


class TestSubClass2(TestClass):
    """Test sub class 2"""

    def test_sub_class_fnc1(self):
        self.memb_var1 = 5

    def sub_class_fnc2(self):
        self.memb_var2 = 5


class _TestClass:
    """Test class"""

    def __init__(self):
        self.memb_var1 = None
        self.memb_var2 = None

    def test_class_fnc1(self):
        self.memb_var1 = 5

    def class_fnc2(self):
        self.memb_var2 = 5


class TestSubMultiClass(TestClass, _TestClass):
    """Test sub multi class"""

    def test_sub_multi_class_fnc1(self):
        self.memb_var1 = 5

    def sub_multi_class_fnc2(self):
        self.memb_var2 = 5


class TestSubSubClass(TestSubMultiClass):
    """Test sub sub class"""

    def test_sub_sub_class_fnc1(self):
        self.memb_var1 = 5

    def sub_sub_class_fnc2(self):
        self.memb_var2 = 5
