"""
    classtree
    ~~~~~~~~~

    Collection of utilities to analyse class tree and show search results within the class tree.

    This package is basically composed of two major parts:

     * ClassTree tools to analyse and search class tree and print the results
     * TreeGraph tools to plot the class tree using plotly

    # Author: Reimund Renner <reimund@siogeen.com>
    :copyright: (c) 2023 by Siogeen.
    :license: BSD, see LICENSE for more details.
"""

from .classtree import ClassTree, TreeGraph
from ._version import VERSION

__version__ = VERSION
