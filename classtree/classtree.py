# coding=utf-8

import inspect
import ast
import re
import types
import importlib
import enum
import warnings

import igraph as ig

class ClassTree():
    """Class tree"""

    def __init__(self, cls, funcs=None, search=None, enums=False, inner_classes=False):
        """Args:
            cls: class, module, class name or module name
            funcs (str): function name reg exp (e.g. '.*myfunc.*')
            search (str): search used functions or variable names by reg exp (e.g. .*myvar.*)
            enums (bool): if True, show also Enum classes
            inner_classes (bool): if True, show also inner classes
        """
        if isinstance(cls, str):
            try:
                mod = importlib.import_module(cls)
                cls = mod
            except ImportError:
                cnames = cls.split('.')
                class_name = cnames[-1]
                mod_name = '.'.join(cnames[:-1])
                mod = importlib.import_module(mod_name)
                cls = getattr(mod, class_name)

        self.name = cls.__name__
        self._tree = ClassTree.class_tree(cls, enums, inner_classes)
        self._classes = ClassTree.get_classes(self._tree)

        self.tree = ClassTree.get_names_tree(self._tree)
        self.func_result = None
        self.search_result = None

        if funcs or search:
            self.search(funcs, search)

    def search(self, funcs=None, search=None):
        """Search class tree

        Args:
            funcs (str or list): (list of) function name reg exp (e.g. '.*myfunc.*')
            search (str or list): search used (list of) functions or variable names by reg exp (e.g. '.*myvar.*')
        """
        if isinstance(funcs, str):
            funcs = [funcs]
        if isinstance(search, str):
            search = [search]
        self.func_result, self.search_result = ClassTree._class_serach(self._tree, funcs, search)

    def get_tree_string(self, indent=4, current_ind=0, funcs=True, search=True):
        """Get class tree string

        Args:
            indent (int): indent of sub classes
            current_ind (int): start indent
            funcs (bool): if True, print found functions
            search (bool): if True, print found search results
        """
        ret = []
        ClassTree._print_tree(self.tree, indent, current_ind,
                          self.func_result if funcs else None,
                          self.search_result if search else None,
                          print_fnc=lambda text: ret.append(text))
        return "\n".join(ret) + '\n'

    def print_tree(self, indent=4, current_ind=0, funcs=True, search=True):
        """Print class tree

        Args:
            indent (int): indent of sub classes
            current_ind (int): start indent
            funcs (bool): if True, print found functions
            search (bool): if True, print found search results
        """
        ClassTree._print_tree(self.tree, indent, current_ind,
                              self.func_result if funcs else None,
                              self.search_result if search else None)

    def save(self, file_name, indent=4, current_ind=0, funcs=True, search=True):
        """Save class tree as text file

        Args:
            file_name (str): file name
            indent (int): indent of sub classes
            current_ind (int): start indent
            funcs (bool): if True, print found functions
            search (bool): if True, print found search results
        """
        with open(file_name, 'w') as file:
            ClassTree._print_tree(self.tree, indent, current_ind,
                              self.func_result if funcs else None,
                              self.search_result if search else None,
                              print_fnc=lambda text: file.write(text+'\n'))

    def analyze(self):
        """Analyze tree

        Returns:
            * list of class names
            * list of depth
            * list of edges
        """
        names, depths = ClassTree._analyze(self.tree)
        edges = ClassTree._get_edges(names, self.tree)
        return (names, depths, edges)

    @staticmethod
    def class_tree(cls, enums=False, inner_classes=False):
        """Get class tree structure

        Args:
            cls: class
            enums (bool): if True, show also Enum classes
            inner_classes (bool): if True, show also inner classes

        Returns:
            class tree as dict of list of dicts
        """
        if isinstance(cls, types.ModuleType):
            return ClassTree._module_tree(cls, enums, inner_classes)

        class_list = {}
        for sub_class in cls.__subclasses__():
            if enums or not ClassTree._is_enum(sub_class):
                class_list.update(ClassTree.class_tree(sub_class, enums, inner_classes))

        if inner_classes:
            for inner_class in cls.__dict__.values():
                if inspect.isclass(inner_class):
                    class_list.update(ClassTree.class_tree(inner_class, enums, inner_classes))

        return { cls: class_list }

    @staticmethod
    def get_names_tree(tree):
        """Get names tree from class treee"""
        ret = {}
        for k, v in tree.items():
            name = k.__name__.split('.')[-1] if isinstance(k, types.ModuleType) else k.__name__
            ret[name] = ClassTree.get_names_tree(v)
        return ret

    @staticmethod
    def get_classes(tree):
        """Get class names of tree structure

        Args:
            tree: class tree structure

        Returns:
            list of class names
        """
        cls = list(tree.keys())
        for sub_trees in tree.values():
            cls += ClassTree.get_classes(sub_trees)
        return cls

    @staticmethod
    def _module_tree(mod, enums=False, inner_classes=False):
        mod_classes = [member for _, member in inspect.getmembers(mod)
                            if ((ClassTree._is_sub_module(mod, member)
                                 or ClassTree._is_module_class(mod, member))
                                 and (enums or not ClassTree._is_enum(member)))]
        # purge classes of sub modules
        for mod_class in tuple(mod_classes):
            if inspect.isclass(mod_class) and inspect.getmodule(mod_class) in mod_classes:
                mod_classes.remove(mod_class)

        class_list = {}
        for mod_class in mod_classes:
            class_list.update(ClassTree.class_tree(mod_class, enums, inner_classes))

        # purge sub classes
        for sub_class in tuple(class_list.keys()):
            if ClassTree._is_sub_class(class_list, sub_class):
                #class_list[sub_class] = {}
                class_list.pop(sub_class)

        # purge multi sub classes
        sub_classes = []
        for cls, sub_tree in class_list.items():
            for sub_class in sub_tree:
                if sub_class not in sub_classes:
                    sub_classes.append(sub_class)
                else:
                    sub_tree[sub_class] = {}

        return { mod: class_list }
        
    @staticmethod
    def _is_sub_module(mod, cls):
        return inspect.ismodule(cls) and (cls.__name__==mod.__name__
            or cls.__name__.startswith(mod.__name__+'.'))

    @staticmethod
    def _is_module_class(mod, cls):
        return inspect.isclass(cls) and ClassTree._is_sub_module(mod, inspect.getmodule(cls))

    @staticmethod
    def _is_enum(cls):
        return isinstance(cls, enum.Enum) or cls.__class__ == enum.EnumType

    @staticmethod
    def _is_sub_class(tree, cls, depth=0):
        ret = False

        for k, sub in tree.items():
            if depth>0 and ((isinstance(k, str) and k==cls.__name__) or k==cls):
                ret = True
            elif sub:
                ret = ClassTree._is_sub_class(sub, cls, depth+1)
            if ret:
                break

        return ret
        
    @staticmethod
    def _class_serach(tree, funcs, search, searchStrings=False, ignore=None):
        ret_funcs = {}
        ret_search = {}
        sub_classes = []
        for cls, sub_tree in tree.items():
            if ignore and cls in ignore:
                continue
            cls_src = inspect.getsource(cls)
            try:
                cls_ast = ast.parse(cls_src)
            except IndentationError:
                continue

            cname = ClassTree._get_class_mod_last_name(cls)

            found_search = {}
            found_funcs = []
            found_inner_search = {}
            found_inner_funcs = {}
            for elem in cls_ast.body:
                ClassTree._ast_funcs(found_search, found_funcs, found_inner_search,
                     found_inner_funcs, cls, elem, funcs, search, searchStrings)

            # ignore multi sub classes
            ignore = []
            for sub_class in sub_tree:
                if sub_class not in sub_classes:
                    sub_classes.append(sub_class)
                else:
                    ignore.append(sub_class)

            sub_funcs, sub_search = ClassTree._class_serach(sub_tree, funcs, search, searchStrings, ignore)

            if found_inner_funcs:
                sub_funcs.update(found_inner_funcs)
            if found_inner_search:
                sub_search.update(found_inner_search)

            if found_funcs or sub_funcs:
                ret_funcs[cname] = (found_funcs, sub_funcs)
            if found_search or sub_search:
                ret_search[cname] = (found_search, sub_search)

        return (ret_funcs, ret_search)

    @staticmethod
    def _get_class_mod_last_name(cls):
        return cls.__name__.split('.')[-1]

    @staticmethod
    def _ast_funcs(found, found_funcs, found_inner_search, found_inner_funcs,
                   cls, elem, funcs, search, searchStrings=False):
        if isinstance(elem, ast.ClassDef):
            if elem.name == ClassTree._get_class_mod_last_name(cls):
                found2 = set()
                for sub in elem.body:
                    if isinstance(sub, ast.ClassDef):
                        # inner class
                        inner_cls = getattr(cls, sub.name)
                        inner_funcs = []
                        inner_search = {}
                        inner_search2 = {}
                        inner_funcs2 = {}
                        ClassTree._ast_funcs(inner_search, inner_funcs, inner_search2, inner_funcs2,
                                             inner_cls, sub, funcs, search, searchStrings)
                        if inner_funcs or inner_funcs2:
                            found_inner_funcs[sub.name] = (inner_funcs, inner_funcs2)
                        if inner_search or inner_search2:
                            found_inner_search[sub.name] = (inner_search, inner_search2)
                    elif isinstance(sub, ast.FunctionDef) or isinstance(sub, ast.AsyncFunctionDef):
                        ClassTree._ast_funcs(found, found_funcs, found_inner_search, found_inner_funcs,
                                             cls, sub, funcs, search, searchStrings)
                    else:
                        ClassTree._ast_serach(found2, sub, search, searchStrings)
                        if found2:
                            pass
        elif isinstance(elem, ast.FunctionDef) or isinstance(elem, ast.AsyncFunctionDef):
            name = elem.name
            ok = ClassTree._match_func_name(elem.name, funcs)
            if not ok:
                inner_name = ClassTree._match_inner_func(elem, funcs)
                if inner_name:
                    name += ':' + inner_name
                    ok = True

            if ok:
                found_funcs.append(name)
            elif search:
                found2 = set()
                for fnc_elem in elem.body:
                    ClassTree._ast_serach(found2, fnc_elem, search, searchStrings)
                if found2:
                    if name not in found:
                        found[name] = []
                    found[name] += found2

    @staticmethod
    def _match_inner_func(elem, funcs):
        if funcs:
            for sub in elem.body:
                if isinstance(sub, ast.FunctionDef) and ClassTree._match_func_name(sub.name, funcs):
                    return sub.name
        return None

    @staticmethod
    def _match_func_name(name, funcs):
        if funcs:
            for func_str in funcs:
                if re.fullmatch(func_str, name, re.IGNORECASE):
                    return True
        return False
        
    @staticmethod
    def _ast_serach(ret_set, elem, search, searchStrings=False):
        if isinstance(elem, list):
            for sub in elem:
                ClassTree._ast_serach(ret_set, sub, search, searchStrings)
        elif isinstance(elem, ast.Return) or isinstance(elem, ast.Expr):
            ClassTree._ast_serach(ret_set, elem.value, search, searchStrings)
        elif isinstance(elem, ast.Call):
            ClassTree._ast_serach(ret_set, elem.func, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.args, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.keywords, search, searchStrings)
        elif isinstance(elem, ast.Attribute):
            ClassTree._ast_find(ret_set, elem.attr, search)
            ClassTree._ast_serach(ret_set, elem.value, search, searchStrings)
            # elem.ctx
        elif isinstance(elem, ast.Constant):
            if searchStrings and isinstance(elem.value, str):
                ClassTree._ast_find(ret_set, elem.value, search)
        elif isinstance(elem, ast.Assign):
            ClassTree._ast_serach(ret_set, elem.value, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.targets, search, searchStrings)
        elif isinstance(elem, ast.Name):
            ClassTree._ast_find(ret_set, elem.id, search)
        elif isinstance(elem, ast.If):
            ClassTree._ast_serach(ret_set, elem.test, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.orelse, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.body, search, searchStrings)
        elif isinstance(elem, ast.IfExp):
            ClassTree._ast_serach(ret_set, elem.test, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.orelse, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.body, search, searchStrings)
        elif isinstance(elem, ast.Compare):
            ClassTree._ast_serach(ret_set, elem.left, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.comparators, search, searchStrings)
            # elem.ops
        elif isinstance(elem, ast.For) or isinstance(elem, ast.AsyncFor):
            ClassTree._ast_serach(ret_set, elem.target, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.iter, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.body, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.orelse, search, searchStrings)
            # elem.type_comment
        elif isinstance(elem, ast.While):
            ClassTree._ast_serach(ret_set, elem.test, search, searchStrings)
            for sub in elem.orelse:
                ClassTree._ast_serach(ret_set, sub, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.body, search, searchStrings)
        elif isinstance(elem, ast.Tuple):
            ClassTree._ast_serach(ret_set, elem.elts, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.dims, search, searchStrings)
            # elem.ctx
        elif isinstance(elem, ast.List):
            ClassTree._ast_serach(ret_set, elem.elts, search, searchStrings)
            # elem.ctx
        elif isinstance(elem, ast.ListComp):
            ClassTree._ast_serach(ret_set, elem.elt, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.generators, search, searchStrings)
        elif isinstance(elem, ast.DictComp):
            ClassTree._ast_serach(ret_set, elem.generators, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.key, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.value, search, searchStrings)
            #todo: test key/value
        elif isinstance(elem, ast.JoinedStr):
            ClassTree._ast_serach(ret_set, elem.values, search, searchStrings)
        elif isinstance(elem, ast.FormattedValue):
            ClassTree._ast_serach(ret_set, elem.value, search, searchStrings)
            # elem.conversion
            # elem.format_spec
        elif isinstance(elem, ast.comprehension):
            ClassTree._ast_serach(ret_set, elem.target, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.iter, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.ifs, search, searchStrings)
            # elem.is_async
        elif isinstance(elem, ast.BoolOp):
            ClassTree._ast_serach(ret_set, elem.op, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.values, search, searchStrings)
        elif isinstance(elem, ast.Try):
            ClassTree._ast_serach(ret_set, elem.handlers, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.body, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.orelse, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.finalbody, search, searchStrings)
        elif isinstance(elem, ast.ExceptHandler):
            ClassTree._ast_serach(ret_set, elem.body, search, searchStrings)
            # elem.name
            # elem.type
        elif isinstance(elem, ast.BinOp):
            ClassTree._ast_serach(ret_set, elem.left, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.right, search, searchStrings)
            # elem.op
        elif isinstance(elem, ast.UnaryOp):
            ClassTree._ast_serach(ret_set, elem.operand, search, searchStrings)
            # elem.op
        elif isinstance(elem, ast.Subscript):
            ClassTree._ast_serach(ret_set, elem.value, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.slice, search, searchStrings)
            # elem.ctx
        elif isinstance(elem, ast.Dict):
            if searchStrings and elem.keys:
                for key in elem.keys:
                    if isinstance(key, str):
                        ClassTree._ast_find(ret_set, key, search)
                for value in elem.values:
                    if isinstance(value, str):
                        ClassTree._ast_find(ret_set, value, search)
        elif isinstance(elem, ast.With) or isinstance(elem, ast.AsyncWith):
            ClassTree._ast_serach(ret_set, elem.items, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.body, search, searchStrings)
            # elem.type_comment
        elif isinstance(elem, ast.withitem):
            ClassTree._ast_serach(ret_set, elem.context_expr, search, searchStrings)
            # elem.optional_vars
        elif isinstance(elem, ast.AugAssign):
            ClassTree._ast_serach(ret_set, elem.target, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.value, search, searchStrings)
            # elem.op
        elif isinstance(elem, ast.FunctionDef):
            ClassTree._ast_find(ret_set, elem.name, search)
            ClassTree._ast_serach(ret_set, elem.decorator_list, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.args, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.body, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.returns, search, searchStrings)
            # elem.type_comment
        elif isinstance(elem, ast.AsyncFunctionDef):
            ClassTree._ast_find(ret_set, elem.name, search)

        elif isinstance(elem, ast.arguments):
            ClassTree._ast_serach(ret_set, elem.args, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.posonlyargs, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.vararg, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.kwarg, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.kwonlyargs, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.defaults, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.kw_defaults, search, searchStrings)
        elif isinstance(elem, ast.Starred):
            ClassTree._ast_serach(ret_set, elem.value, search, searchStrings)
            # elem.ctx
        elif isinstance(elem, ast.GeneratorExp):
            ClassTree._ast_serach(ret_set, elem.elt, search, searchStrings)
            ClassTree._ast_serach(ret_set, elem.generators, search, searchStrings)
        elif isinstance(elem, ast.ClassDef):
            ClassTree._ast_serach(ret_set, elem.body, search, searchStrings)
            # elem.bases
            # elem.keywords
            # elem.name
        elif isinstance(elem, ast.Await):
            ClassTree._ast_serach(ret_set, elem.value, search, searchStrings)
        elif isinstance(elem, ast.Raise) or isinstance(elem, types.NoneType) \
        or isinstance(elem, ast.And) or isinstance(elem, ast.Or) \
        or isinstance(elem, ast.Pass) or isinstance(elem, ast.Break) \
        or isinstance(elem, ast.Continue) or isinstance(elem, ast.arg) \
        or isinstance(elem, ast.keyword) or isinstance(elem, ast.Slice) \
        or isinstance(elem, ast.Import) or isinstance(elem, ast.ImportFrom):
            pass
        else:
            warnings.warn(f'Unknown elem: {type(elem)}')

    @staticmethod
    def _ast_find(ret_set, text, search):
        for search_str in search:
            if re.fullmatch(search_str, text, re.IGNORECASE):
                ret_set.add(text)
                break

    @staticmethod
    def _print_tree(tree, indent=4, current_ind=0, funcs=None, search=None, print_fnc=print):
        if tree:
            cls_names = set(tree.keys())
        else:
            cls_names = set()
        if funcs:
            cls_names = cls_names.union(set(funcs.keys()))
        if search:
            cls_names = cls_names.union(set(search.keys()))

        for cls_name in cls_names:
            sub_tree = tree.get(cls_name)
            if current_ind:
                ClassTree._print_dashed(current_ind - indent, indent, cls_name, print_fnc)
            else:
                print_fnc(cls_name)

            cls_funcs = funcs.get(cls_name) if funcs else None
            cls_search = search.get(cls_name) if search else None
            sub_funcs = cls_funcs[1] if cls_funcs else None
            sub_search = cls_search[1] if cls_search else None
            ClassTree._print_tree(sub_tree, indent, current_ind + indent, sub_funcs, sub_search, print_fnc)

            if cls_funcs and cls_funcs[0]:
                func_names = cls_funcs[0]
                for func_name in func_names:
                    ClassTree._print_dashed(current_ind, indent, 'F: ' + func_name, print_fnc)

            if cls_search and cls_search[0]:
                search_res = cls_search[0]
                for func_name, func_results in search_res.items():
                    ClassTree._print_dashed(current_ind, indent, 'S: ' + func_name, print_fnc)
                    for func_res in func_results:
                        ClassTree._print_dashed(current_ind+indent+3, indent, func_res, print_fnc)

    @staticmethod
    def _print_dashed(before_dashes, indent, text, print_fnc=print):
        print_fnc(' ' * before_dashes + '└' + '-'*(indent-1) + text)

    @staticmethod
    def _get_edges(names, tree, edge_index=-1):
        edges = []
        for k, sub_tree in tree.items():
            ik = names.index(k)
            if edge_index >= 0:
                edges.append((edge_index, ik))

            edges += ClassTree._get_edges(names, sub_tree, ik)

        return edges

    @staticmethod
    def _analyze(tree, depth=0.0):
        # edges: 01 02 23 24 05 56 57
        names = list(tree.keys())
        depths = [depth] * len(names)

        vdepth = depth + 1.0
        for val in tree.values():
            vnames, vdepths = ClassTree._analyze(val, vdepth)
            for name, depth in zip(vnames, vdepths):
                if name not in names:
                    names.append(name)
                    depths.append(depth)

        return (names, depths)


class TreeGraph():
    """Tree graph"""

    def __init__(self, tree, layout='tree', plotlib='plotly'):
        """Args:
            tree (ClassTree): class tree
            layout (str): graph layout ('tree', 'auto', ...)
            plotlib (str): plot library ('plotly', 'matplotlib' or 'cairo')
        """
        self.name = tree.name
        self.layout = layout
        self.plotlib = plotlib

        self.plotly_fig = {}
        self.matplotlib_fig = {}

        self.graph, self.labels, self.max_depth, self.edges = TreeGraph._tree_graph(tree)

    def save(self, path, layout=None, plotlib=None, **kwargs):
        """Save graph to file

        Args:
            path (str): file path
            layout (str): graph layout ('tree', 'auto', ...)
            plotlib (str): plot library ('plotly', 'matplotlib' or 'cairo')
            kwargs: additional parameters, e.g. width, height, ...
        """
        fig, layout, plotlib = self._get_fig(layout, plotlib)
        if plotlib=='plotly':
            fig.write_image(path, **kwargs)
        elif plotlib=='matplotlib':
            ig.plot(self.graph, layout=layout, target=fig.gca())
            fig.savefig(path)
        elif plotlib=='cairo':
            ig.plot(self.graph, layout=layout, target=path)

    def plot(self, layout=None, plotlib=None):
        """Plot graph

        Args:
            layout (str): graph layout ('tree', 'auto', ...)
            plotlib (str): plot library ('plotly', 'matplotlib' or 'cairo')
        """
        fig, layout, plotlib = self._get_fig(layout, plotlib)
        if plotlib=='plotly':
            fig.show()
        elif plotlib=='matplotlib':
            ig.plot(self.graph, layout=layout, target=fig.gca())
        elif plotlib=='cairo':
            ig.plot(self.graph, layout=layout)

    def _get_fig(self, layout=None, plotlib=None):
        if not layout:
            layout = self.layout
        if not plotlib:
            plotlib = self.plotlib

        if plotlib=='plotly':
            fig = self.plotly_fig.get(layout)
            if not fig:
                self.plotly_fig[layout] = fig = TreeGraph._gen_plotly_fig(self.name,
                    self.graph, self.max_depth, layout)
        elif plotlib=='matplotlib':
            fig = self.matplotlib_fig.get(layout)
            if not fig:
                self.matplotlib_fig[layout] = fig = TreeGraph._gen_matplotlib_fig()
        elif plotlib=='cairo':
            fig = None
        else:
            raise ValueError("Unknown plotlib specified")

        return (fig, layout, plotlib)

    @staticmethod
    def _gen_matplotlib_fig():
        import matplotlib.pyplot as plt
        fig, _ = plt.subplots()
        return fig

    @staticmethod
    def _gen_plotly_fig(name, graph, max_depth, layout='tree'):
        import plotly.graph_objects as go

        lay = graph.layout(layout)
        labels = graph.vs["label"]
        nr_vertices = len(labels)
        position = {k: lay[k] for k in range(nr_vertices)}
        M = max_depth
        
        edges = [e.tuple for e in graph.es]
        
        L = len(position)
        Xn = [position[k][0] for k in range(L)]
        Yn = [2*M-position[k][1] for k in range(L)]
        Xe = []
        Ye = []
        for edge in edges:
            Xe+=[position[edge[0]][0],position[edge[1]][0], None]
            Ye+=[2*M-position[edge[0]][1],2*M-position[edge[1]][1], None]
        
        fig = go.Figure()
        fig.add_trace(go.Scatter(x=Xe,
                           y=Ye,
                           mode='lines',
                           line=dict(color='rgb(210,210,210)', width=1),
                           hoverinfo='none'
                           ))
        fig.add_trace(go.Scatter(x=Xn,
                          y=Yn,
                          mode='text', # 'markers'
                          name='class',
                          marker=dict(symbol='circle-dot',
                                        size=18,
                                        color='#6175c1',    #'#DB4551',
                                        line=dict(color='rgb(50,50,50)', width=1)
                                        ),
                          text=labels,
                          hoverinfo='text',
                          opacity=0.8
                          ))
        axis = dict(showline=False, # hide axis line, grid, ticklabels and  title
                    zeroline=False,
                    showgrid=False,
                    showticklabels=False,
                    )
        fig.update_layout(title=f'Class Tree of "{name}"',
                      #annotations=self._make_annotations(position, labels),
                      font_size=12*2,
                      showlegend=False,
                      xaxis=axis,
                      yaxis=axis,
                      margin=dict(l=40, r=40, b=85, t=100),
                      hovermode='closest',
                      plot_bgcolor='rgb(248,248,248)'
                      )
        return fig

    def _make_annotations(self, pos, text, font_size=10, font_color='rgb(250,250,250)'):
        return None
        L = len(pos)
        if len(text) != L:
            raise ValueError('The lists pos and text must have the same len')
        annotations = []
        for k in range(L):
            annotations.append(
                dict(
                    text=self.labels[k], # or replace labels with a different list for the text within the circle
                    x=pos[k][0], y=2*self.M-self.position[k][1],
                    xref='x1', yref='y1',
                    font=dict(color=font_color, size=font_size),
                    showarrow=False)
            )
        return annotations

    @staticmethod
    def _tree_graph(tree):
        v_label, Y, edges = tree.analyze()
        nr_vertices = len(v_label)
        graph = ig.Graph(n=nr_vertices, edges=edges, directed=True)
        graph.vs["name"] = v_label
        graph.vs["label"] = v_label
        return (graph, v_label, max(Y), edges)
