"""
Command line interface implementation.
"""
import argparse
import os

from .classtree import ClassTree, TreeGraph
from ._version import VERSION


def parse_arguments():
    """Parse and handle CLI arguments."""
    parser = argparse.ArgumentParser(
        prog='classtree',
        description='Python class tree search and analysis',
        epilog='Example: %(prog)s --print --save png mymod.myclass1 mymod.myclass2')

    parser.add_argument("classes", metavar='CLASS', nargs='+',
                        help="class or module names (e.g. plotly.tools.FigureFactory)")
    parser.add_argument("--funcs", metavar='REGEXP',
                        help="function search regular expression (e.g. .*test.*)")
    parser.add_argument("--search", metavar='REGEXP',
                        help="tree search regular expression (e.g. .*myvar.*)")
    parser.add_argument("--print", action='store_true', help="print tree (default)")
    parser.add_argument("--plot", action='store_true', help="plot tree (needs plotly)")
    parser.add_argument("--layout", action='append', default=['tree'],
                        help="plot/save graph layout ('tree', 'auto', ...)")
    parser.add_argument("-s", "--save", metavar='EXT', action='append', default=[],
        help="save tree to file extension (txt, png, jpeg, ...)")
    parser.add_argument("--path", default="", help="specify save path")
    parser.add_argument("--saveargs", default='',
                        help="save arguments (e.g. width=1000,height=500)")
    parser.add_argument('--version', action='version', version=VERSION)

    return parser.parse_args()


def dispatch(args):
    """Execute functionality requested through the CLI."""
    trees = [ClassTree(cls, funcs=args.funcs, search=args.search) for cls in args.classes]
    for tree in trees:
        if args.print or (not args.plot and not args.save):
            tree.print_tree()
        saveimges = set(args.save) 
        if 'txt' in args.save:
            tree.save(os.path.join(args.path, tree.name + '.txt'))
            saveimges.remove('txt')

        if args.plot or saveimges:
            tg = TreeGraph(tree)
            for layout in args.layout:
                if args.plot:
                    tg.plot(layout=layout)
                saveargs = eval(f'dict({args.saveargs})')
                for save_ext in saveimges:
                    tg.save(os.path.join(args.path, f'{tree.name}_{layout}.{save_ext}'),
                            layout=layout, **saveargs)


def main():
    """classtree."""
    args = parse_arguments()
    dispatch(args)
