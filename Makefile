
test: test_classtree test_cli

test_classtree:
	PYTHONPATH=tests python3 -m unittest test_classtree

test_cli:
	PYTHONPATH=tests python3 -m unittest test_cli
